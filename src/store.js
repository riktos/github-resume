import createReducer from './redusers';
import thunk from 'redux-thunk';
import { createStore, applyMiddleware, compose } from 'redux';
import api from './api';

let preState = {};

export default function (store) {
  const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
  return createStore(
    createReducer(store),
    preState,
    composeEnhancers(applyMiddleware(thunk.withExtraArgument({ api }))));
}
