import { combineReducers } from "redux";

import git from "./search";

export default () =>
  combineReducers({
    git
  });
