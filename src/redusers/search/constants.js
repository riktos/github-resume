export const SEARCH = "search/start";
export const SEARCH_SUCCESS = "search/success";
export const SEARCH_ERROR = "search/error";