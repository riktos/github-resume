import { SEARCH, SEARCH_SUCCESS, SEARCH_ERROR } from "./constants";
const initialState = {
  search: {
    isLoading: false,
    payload: null,
    errors: {}
  }
};

export default function(state = initialState, action) {
  switch (action.type) {
    case SEARCH:
      return {
        ...state,
        search: { isLoading: true, payload: null, errors: {} }
      };
    case SEARCH_SUCCESS:
      return {
        ...state,
        search: { isLoading: false, payload: action.payload, errors: {} }
      };
    case SEARCH_ERROR:
      return {
        ...state,
        search: { isLoading: false, payload: null, errors: action.payload }
      };
    default:
      return state;
  }
}
