import { SEARCH, SEARCH_SUCCESS, SEARCH_ERROR } from "./constants";

const getUserRepos = userData => {
  return (dispatch, getState, { api }) => {
    api.getPopularRepos(userData.login).then(res => {
      const repos = res.data.items;
      if (repos.length === 0) {
        dispatch({
          type: SEARCH_SUCCESS,
          payload: {
            user: userData,
            repos: null,
            language: null
          }
        });
      } else {
        api
          .getAllRepos(userData.login)
          .then(allRepos => {
            const languages = {};
            allRepos.data.forEach(repo => {
              if (repo.language in languages) {
                languages[repo.language] += 1;
              } else {
                if (repo.language) {
                  languages[repo.language] = 1;
                }
              }
            });
            const total = allRepos.data.length;
            for (let key in languages) {
              languages[key] = Math.ceil(100 * (languages[key] / total));
            }
            dispatch({
              type: SEARCH_SUCCESS,
              payload: {
                user: userData,
                repos: repos,
                language: languages
              }
            });
          })
          .catch(err => {
            dispatch({
              type: SEARCH_ERROR,
              payload: err
            });
          });
      }
    });
  };
};

export const searchUser = username => (dispatch, getState, { api }) => {
  const errors = {};

  dispatch({
    type: SEARCH
  });

  if (!username) {
    errors.username = "Username is empty";
    dispatch({
      type: SEARCH_ERROR,
      payload: errors
    });
  }
  api
    .search(username)
    .then(user => {
      dispatch(getUserRepos(user.data));
    })
    .catch(err => {
      errors.username = "User is not found";
      dispatch({
        type: SEARCH_ERROR,
        payload: errors
      });
    });
};
