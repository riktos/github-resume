import React from "react";

import { Provider } from "react-redux";
import createStore from "./store";

import Search from "./components/Search";
import UserInfo from "./components/UserInfo";

import "./styles/index.scss";
const store = createStore();

function App() {
  return (
    <div className="App">
      <Provider store={store}>
        <div className="App__header">
          <span>exozet</span>
        </div>
        <span className="App__title">My Github Resumé</span>
        <Search />
        <UserInfo />
        <div className="App__footer">
          <span>© exozet 2019</span>
        </div>
      </Provider>
    </div>
  );
}

export default App;
