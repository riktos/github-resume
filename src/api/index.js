import axios from "axios";

export default {
  search(name) {
    return axios.get(`https://api.github.com/users/${name}`);
  },
  getPopularRepos(name) {
    return axios.get(
      `https://api.github.com/search/repositories?q=user%3A${name}&sort=stars&order=desc&page=1&per_page=5`
    );
  },
  getAllRepos(name) {
    return axios.get(`https://api.github.com/users/${name}/repos`);
  }
};
