import React, { useMemo } from "react";
import "./style.scss";

const Repo = props => {
  const { created_at, updated_at, language, privateRepo } = useMemo(() => {
    const created_at = new Date(props.repo.created_at).getFullYear();
    const updated_at = new Date(props.repo.updated_at).getFullYear();
    const language = props.repo.language ? props.repo.language : "Other";
    const privateRepo = props.repo.private ? "Private" : "Public";

    return {
      created_at,
      updated_at,
      language,
      privateRepo
    };
  }, [props]);
  return (
    <div className="Repo">
      <div className="Repo__row__header">
        <div className="Repo__name">
        <a 
          href={props.repo.html_url} 
          className="Repo__name__link" 
          target="_blank" 
          rel="noopener noreferrer"
        >
          {props.repo.full_name}</a>
          </div>
        <div className="Repo__time">
          {created_at} – {updated_at}
        </div>
      </div>
      <div className="Repo__row">
        <span className="Repo__lang-rights">
          {language} – {privateRepo}
        </span>
      </div>
      <div className="Repo__row__description">{props.repo.description}</div>
      <div className="Repo__row">
        <span className="Repo__row__footer">
          This repository has <b>{props.repo.stargazers_count} stars</b> and
          &nbsp;<b>{props.repo.forks_count} forks</b>. If you would like more
          information about this repository and my contributed code, please
          visit <a href={props.repo.html_url} className="Repo__row__footer__link" target="_blank" rel="noopener noreferrer">the repo</a>
          &nbsp; on GitHub.
        </span>
      </div>
    </div>
  );
};
export default Repo;
