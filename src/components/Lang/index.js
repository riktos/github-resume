import React from "react";
import "./style.scss";

const Lang = props => {
  return (
    <div className="Lang">
      <div className="Lang__row">
        <a
          href={`https://github.com/search?q=user%3A${props.userName}&l=${
            props.langName
          }`}
          className="Lang__name"
          target="_blank"
          rel="noopener noreferrer"
        >
          {props.langName}
        </a>
        <span className="Lang__lvl">{props.value}%</span>
      </div>
      <div class="progressbar">
        <span style={{width: `${props.value}%`}} />
      </div>
    </div>
  );
};
export default Lang;
