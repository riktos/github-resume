import React, { useState, useCallback } from "react";
import { searchUser } from "../../redusers/search/action";
import { useDispatch, useSelector } from "react-redux";
import "./style.scss";

const Search = () => {
  const dispatch = useDispatch();
  const { errors } = useSelector(state => state.git.search);
  const [userName, setUserName] = useState("");

  const onChange = useCallback(e => {
    setUserName(e.target.value);
  }, []);
  const onSubmit = () => {
    dispatch(searchUser(userName));
  };

  return (
    <div className="Search">
      <div className="Search__row">
        <span className="Search__row__header">Github username</span>
      </div>
      <div className="Search__row">
        <input
          className="Search__row__input"
          name="username"
          type="text"
          placeholder="John Doe"
          onChange={onChange}
        />
        <button
          className="Search__row__button"
          type="button"
          onClick={onSubmit}
        >
          Generieren
        </button>
      </div>
      <div className="Search__row__error">{errors.username}</div>
    </div>
  );
};

export default Search;
