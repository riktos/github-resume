import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import Lang from "../Lang";
import Repo from "../Repo";
import "./style.scss";

const UserInfo = () => {
  const { payload } = useSelector(state => state.git.search);
  const [langList, setLangList] = useState([]);
  const [reposList, setReposList] = useState([]);
  const [userData, setUserData] = useState({
    name: "User Name",
    login: null,
    status: "User Status",
    followers: "XXX",
    public_repos: "XXX",
    created_year: "YEAR",
    url: "https://www.example.com",
    location: null
  });

  useEffect(() => {
    let localLangList = [];
    if (payload) {
      setUserData({
        name: payload.user.name || payload.user.login,
        login: payload.user.login,
        status: payload.user.bio,
        url: payload.user.html_url,
        followers: payload.user.followers,
        public_repos: payload.user.public_repos,
        created_year: new Date(payload.user.created_at).getFullYear(),
        location: payload.user.location
      });

      for (let key in payload.language) {
        if ((key !== "total") | (key === "other")) {
          localLangList.push({ name: key, value: payload.language[key] });
        }
      }
      if (payload.repos) {
        setReposList(payload.repos);
      }
      if (payload.language) {
        setLangList(localLangList);
      }
    }
  }, [payload]);

  return (
    <div className="UserInfo">
      <div className="UserInfo__header container">
        <h1 className="UserInfo__header__name">{userData.name}</h1>
        <div className="UserInfo__header__status">{userData.status}</div>
        <div className="UserInfo__header__url">
          <a href={userData.url} target="_blank" rel="noopener noreferrer">
            {userData.url}
          </a>
        </div>

        <span className="UserInfo__header__description">
          On GitHub since {userData.created_year}, {userData.name} is a&nbsp;
          developer {userData.location ? `based in ${userData.location}` : ""}
          with&nbsp;
          <a
            className="UserInfo__header__link"
            href={`https://github.com/${userData.login}?tab=repositories`}
            target="_blank"
            rel="noopener noreferrer"
          >
            {userData.public_repos} public repositories
          </a>
          &nbsp;and&nbsp;
          <a
            className="UserInfo__header__link"
            href={`https://github.com/${userData.login}/followers`}
            target="_blank"
            rel="noopener noreferrer"
          >
            {userData.followers} followers.
          </a>
        </span>
      </div>
      <div className="UserInfo__languages container">
        <div className="UserInfo__languages header">Languages</div>
        {console.log(userData.login)}
        <div className="UserInfo__languages__body">
          {langList.map((lang, i) => (
            <Lang
              langName={lang.name}
              value={lang.value}
              userName={userData.login}
              key={i}
            />
          ))}
        </div>
      </div>
      <div className="UserInfo__repo container">
        <div className="UserInfo__repo header">Popular Repositories</div>
        {reposList.map((repo, i) => (
          <Repo repo={repo} key={i} />
        ))}
      </div>
    </div>
  );
};

export default UserInfo;
